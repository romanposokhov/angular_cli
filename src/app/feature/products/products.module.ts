import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsComponent } from './products.component';
import { InputModule } from 'src/app/shared/input/input.module';
import { ButtonModule } from 'src/app/shared/button/button.module';
import { ProductModule } from './product/product.module';


@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    InputModule,
    ButtonModule,
    ProductModule
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule { }
